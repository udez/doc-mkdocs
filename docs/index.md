# Apprendre à créer un site statique en markdown
Sur cette documentation vous allez apprendre à créer un site à l'aide des Mkdocs.
un mkdocs est un outils très puissant pour faire des sites rapidements tels que des blogs, un site vitrine
ou encore pour faire une documentation pour vos projets.


## Le mkdocs ?

Mkdocs c'est un generateur de site statique rapide et facile à mettre en place.
Les pages sont rédigées au format Markdown et la configuration au format [YAML](https://fr.wikipedia.org/wiki/YAML).
La phase de création des pages peut se faire en mode live, et la génération se build très rapidement en une ligne de commande.

La documentation générée possède un menu, une barre de recherche, et plusieurs thèmes disponibles.

## Le markdown
Markdown est un langage de balisage léger [créé en 2004 par John Gruber avec l'aide d' Aaron Swartz](https://fr.wikipedia.org/wiki/Markdown). Son but est d'offrir une syntaxe facile à lire et à écrire, comme une alternative au langage [Html](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language). Le markdown est utilisé dans beaucoup de documentation, blog etc..., et c'est le langage qu'ont va utiliser dans mkdocs.
