# Mkdocs Tuto

Pour installer mkdocs vous avez besoin d'avoir python installé sur votre ordinateur et si vous utilisez la dernière
version de python vous aurez très souvent le gestionnaire de paquet **pip** installer par défaut.
Sinon pour install: **pip**


`pip install --upgrade pip`

Si vous installez pip pour la première 

```bash
python get-pip.py
```

Pour installer Mkdocs
```
 pip install mkdocs
```

Taper `mkdocs --version`  pour voir si mkdocs est installer correctement
________________________________________________________________________________

## Débuter avec mkdocs 

### Installation

Pour créer un nouveau projet mkdocs, taper:

`mkdocs new <nom_du_projet>`

<!-- ![screenshoot de visual code](image/screenFichierdeBase.png) -->

Les fichiers de base qui son générer lors de l'initialisation
-
![screenshoot de visual code](image/initial-layout.png)

- `mkdocs.yml` : dossier de configuration
- `docs/` : c'est le dossier qui contient les fichiers markdown
- `index.md` : c'est l'index du site

## Lancer le serveur

Pour lancer le serveur, aticver l'environnement virtuel avec `pipenv shell` et lancer  le serveur avec `mkdocs serve`.

## Choisir son thèmes

### Thèmes par défaut
![thème par defaut](image/defaultheme.png)

Il y a des thèmes à dipostion préinstallé comme par example le thème par défaut et le thème  ***readthedocs*** que vous pouvez utiliser directement en tapant 
`theme: readthedocs` dans le fichier ***mkdocs.yml*** pour le thème readthedocs

### Readthedocs thème
![thème par defaut](image/readthedocs.png)

### les thèmes externes téléchargeable via Pip

Par example pour installé le thème Material

Il faut installer la dernière version de Material
`
pipenv install mkdocs-material
`
Et ajouter le code suivant dans le fichier ***mkdocs.yml***

`
theme:
  name: 'material'
`
> thème material
![thème par defaut](image/material.png)

Pour voir les autres [ thèmes ](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes).



## Pour customiser plus les  thèmes

Vous pouvez aussi  créer un fichier ***css*** dans le dossier ***docs*** 
et mettre le lien  `extra_css: ['styles/<nom_du_fichier_css>.css']` dans le fichier ***mkdocs.yml***.

Par Example pour changer certaines couleurs (menu du haut, sommaire des pages) du site vous ajouter le code suivant:
```
.navbar {
  background-image: none;
  background-color: rebeccapurple;
}

.navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:hover, .navbar-default .navbar-nav>.active>a:focus {
  color: rebeccapurple;
  background-color: #fff;
}

.navbar-default .navbar-nav>li>a:hover, .navbar-default .navbar-nav>li>a:focus{
  color: rebeccapurple;
  background-color: #e5e5e5;
}

.well {
  background-color: rebeccapurple;
  font-size: larger;
}

.bs-sidebar .nav > li > a, ul.nav .main {
  color: white;
}

ul.nav .main:hover, .active:hover {
  background-color: white
}
.bs-sidebar .nav > li > a:hover {
  color: rebeccapurple;
}
```

Vous avez le lien de la documention officielle si vous voulez en savoir sur le sujet:
[Doc customisation de thèmes](https://www.mkdocs.org/user-guide/custom-themes/ "Texte pour le titre, facultatif")