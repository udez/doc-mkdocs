# Git

## Qu'est ce que Git ?


> Est-ce que github, gitlab ?

## Les commandes principales de Git

### git [clone (copier)](https://git-scm.com/docs/git-clone/fr) 
- Cloner un dépôt dans un nouveau répertoire

```
 git clone https://github.com/libgit2/libgit2
```

### git [checkout](https://git-scm.com/docs/git-checkout/fr)
- `git checkout -b <nom_branche>` : Création et basculement d'une nouvelle branche
- `git checkout <nom_branche>` : Basculer vers la branche `<nom_branche>`

```  
$> git branch
master
ma_branche
feature_inprogress_branch
$> git checkout ma_branche
```

### git [commit](https://git-scm.com/docs/git-commit)
- Pour noté les modifications fait

```
git commit –m “Description du commit”
```

### git [push](https://git-scm.com/docs/git-push)
 - Pour envoyer les modifications fait sur le repo github du projet

```
 git push origin master
 git push 
```

### git [pull](https://git-scm.com/docs/git-pull)
- Pour récupérer les modifications fait par une autre personne
``` 
git checkout master
git pull origin master
```

_________________________________________________________________________________________________________________________

## Construire le site

Ça a l'air bien. Vous êtes prêt à déployer la première page de votre documentation. Construisez d'abord la documentation:
```
mkdocs build
```
Cela va créer un nouveau répertoire, nommé site. Jetez un œil à l'intérieur du répertoire:
```
$ ls site
about  fonts  index.html  license  search.html
css    img    js          mkdocs   sitemap.xml
```

Si vous utilisez un contrôle de code source tel que git, vous ne voudrez probablement pas vérifier vos builds de documentation dans le référentiel. Ajoutez une ligne contenant site / à votre fichier .gitignore.

`echo "site/" >> .gitignore`

Après un certain temps, les fichiers peuvent être supprimés de la documentation mais ils résideront toujours dans le répertoire du site. Pour supprimer ces fichiers périmés, exécutez simplement mkdocs avec le commutateur --clean

`mkdocs build --clean`

