# La syntaxe

### Les paragraphes

Un paragraphe est une ou plusieurs lignes de texte. En Markdown, pour terminer un paragraphe, créez une ligne vide avant de commencer le suivant.

```
Ceci est un paragraphe.

Encore un !
```

### En gras

La mise en forme d'un mot en gras est simple. Il suffit de l'entourer avec deux astérisques (*).

``` 
**Phrase en gras**
```
ou encore de deux underscore

```
__Phrase en gras__
```

### Les titres

En Markdown, 6 niveaux de titres sont disponibles. Plusieurs méthodes sont possibles.
Pour la première, il faut précéder le titre avec un dièse (#).

```
# Titre de niveau 1
## Titre de niveau 2
### Titre de niveau 3
#### Titre de niveau 4
##### Titre de niveau 5
###### Titre de niveau 6
```

### Les listes

Vous pouvez faire des listes à puces
```
* Item 1
* Item 2
* Item 3
```
Pour des listes numérotées (ordonnées) :
```
1. Item 1
2. Item 2
3. Item 3
```

### Les liens

Voici la syntaxe pour créer des liens :

```
[Texte du lien](http://www.google.fr "Titre alternatif pour les non voyants")
```

### Les liens par référence

Au lieu d'insérer l'URL du lien directement, vous pouvez utiliser les références si vous devez utiliser l'URL régulièrement dans le document (ou pour plus de lisibilité).

```
Rendez-vous sur [Texte du lien][monsite] !

[monsite]: https://udez.gitlab.io/doc-mkdocs/ "Titre alternatif, utile pour les non voyants"
```

Vous pouvez insérer les références où vous voulez, le plus lisible étant en bas de document.

### Les images

La syntaxe pour insérer une image est semblable à celle pour créer un lien :

```
![Texte alternatif](http://www.monsite.fr/image.png "Texte pour le titre, facultatif")
```

### Les images par référence

A la manière des liens, vous pouvez insérer des images en utilisant les références.

```
![Text alternatif][img-monsite]

[img-monsite]: http://www.monsite.fr/image.png "Titre facultatif"
```
### Les citations
Pour les citations, c'est également tout simple ! Vous devez précéder les lignes d'un chevron ">" :

```
> Ceci est une citation
```