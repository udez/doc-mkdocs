## Mise en ligne

### Github

Vous avez plusieurs possibilité qui s'offrent a vous mais celle que je préfère le plus est l'herbegément sur github pages: 
[github pages](https://pages.github.com/"Texte pour le titre, facultatif")

## Déploiement sur GitLab

```yml
image: python:alpine
# recupère la dépendance python docker basé sur alpine 

before_script:
  - pip install pipenv
# installe pipenv => environnement virtuel 
  - pipenv install 
# installe les différentes dependance dans le "Pipfile.lock"

pages:
  script:
  - pipenv run mkdocs build
# lance et build le dossier mkdocs
  - mv site public
# deplace le dossier "site" généré par mkdocs build dans public
  artifacts:
    paths:
    - public
# indique le chemin public
  only:
  - master
# prend en compte juste la branche master 
```